import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashSet;
import java.util.Set;

public class PongKeyListener implements KeyListener {

    // Set of currently keysPressed keys
    final Set<Integer> keysPressed = new HashSet<>();

    @Override
    public synchronized void keyPressed(KeyEvent e) {
        //Add key to set
        keysPressed.add(e.getKeyCode());
    }

    @Override
    public synchronized void keyReleased(KeyEvent e) {
        //Remove key from set
        keysPressed.remove(e.getKeyCode());
    }

    @Override
    public void keyTyped(KeyEvent e) {/* Not used */ }
}
