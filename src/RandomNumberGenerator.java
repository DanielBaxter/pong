import java.util.Random;

public class RandomNumberGenerator {

    private static Random rand;

    static void init() {
        rand =  new Random();
    }

    static Boolean randomBoolean() {
        return rand.nextBoolean();
    }
}
