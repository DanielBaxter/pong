import javax.swing.JFrame;

public class Main {

    private static final int WINDOW_WIDTH = 1000;
    private static final int WINDOW_HEIGHT = 500;

    public static void main(String[] args) {
        JFrame frm = new JFrame("Pong");
        Game g = new Game();
        frm.setContentPane(g);
        frm.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        frm.setResizable(false);
        frm.setVisible(true);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
