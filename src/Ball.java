import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.util.Set;

import static java.lang.Thread.sleep;

//Ball inherits x/y/width/height/initX/initY from Entity
public class Ball extends Entity {

    private float initVelocity = 1.5f;

    private float velocityX = initVelocity;
    private float velocityY = initVelocity;

    private boolean inTimeout = false;

    private static final float maxVelocity = 30;
    private static final float acceleration = 0.0005f;

    private boolean canBouncePaddle = true;

    //Constructor - this initialises the object with the values passed to it in the brackets
    Ball(float x, float y, float width, float height) {
        super(x, y, width, height);
        if (RandomNumberGenerator.randomBoolean())
            velocityX = -velocityX;
        if (RandomNumberGenerator.randomBoolean())
            velocityY = -velocityY;
    }

    @Override
    protected void draw(Graphics2D g) {
        //Draws a circle at the paddle's x/y and it's diameter
        Ellipse2D ball = new Ellipse2D.Double(x, y, width, height);
        g.fill(ball);
    }

    @Override
    protected void update(CollisionManager collisionManager, Set<Integer> keysPressed) {
        //Don't update if in timeout
        if (inTimeout) return;

        //Bounce ball off top/bottom walls
        if (collisionManager.collidingWithTopWall(this) || collisionManager.collidingWithBottomWall(this))
            velocityY = -velocityY;

        //Move ball
        x += velocityX;
        y += velocityY;

        //Gradually accelerate ball
        if (velocityX > 0)
            velocityX = velocityX + acceleration;
        else
            velocityX = velocityX - acceleration;

        if (velocityY > 0)
            velocityY = velocityY + acceleration;
        else
            velocityY = velocityY - acceleration;

        //Cap velocity so game doesn't become too difficult
        if (velocityX > maxVelocity)
            velocityX = maxVelocity;
        if (velocityY > maxVelocity)
            velocityY = maxVelocity;
    }

    //Flipping velocity makes the ball effectively 'bounce' when called
    void bounceX() {
        if (canBouncePaddle) {
            velocityX = -velocityX;
            canBounceAfterTimout(10);
            canBouncePaddle = false;
        }
    }

    void reset() {
        //Reset position and velocity
        x = initX;
        y = initY;
        velocityX = velocityY = 0;

        //After 2 seconds (2000 milliseconds) ball can start moving again
        new Thread(() -> {
            try {
                inTimeout = true;
                sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            velocityX = RandomNumberGenerator.randomBoolean() ? initVelocity : -initVelocity;
            velocityY = RandomNumberGenerator.randomBoolean() ? initVelocity : -initVelocity;
            inTimeout = false;
        }).start();
    }

    private void canBounceAfterTimout(int time) {
        new Thread(() -> {
            try {
                sleep(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            canBouncePaddle = true;
        }).start();
    }
}
