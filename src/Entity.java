import java.awt.*;
import java.util.Set;

public abstract class Entity {

    //Variables (or bits of information really) about this object
    //All entities should have a position and width/height
    float x, y;
    float initX, initY;
    float width, height;

    Entity(float x, float y, float width, float height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.initX = x;
        this.initY = y;
    }

    protected abstract void draw(Graphics2D g);

    protected abstract void update(CollisionManager collisionManager, Set<Integer> keysPressed);
}
