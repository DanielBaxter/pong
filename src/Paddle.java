import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.Set;

//Paddle inherits x/y/initX/initY/width/height from Entity
public class Paddle extends Entity {

    private static final float velocity = 3;

    //The keys that makes this paddle object move up and down
    private int upKey;
    private int downKey;

    //Constructor - this 'makes' the object with values you pass to it
    Paddle(float x, float y, float width, float height, int upKey, int downKey) {
        super(x, y, width, height);
        this.upKey = upKey;
        this.downKey = downKey;
    }

    @Override
    protected void draw(Graphics2D g) {
        //Draws a rectangle at the paddle's x/y and it's dimensions width/height
        Rectangle2D paddle = new Rectangle2D.Double(x, y, width, height);
        g.fill(paddle);
    }

    @Override
    protected void update(CollisionManager collisionManager, Set<Integer> keysPressed) {

        //Check if this paddle's move keys are being pressed
        boolean movingUp = keysPressed.contains(upKey) && !collisionManager.collidingWithTopWall(this);
        boolean movingDown = keysPressed.contains(downKey) && !collisionManager.collidingWithBottomWall(this);

        //Move paddle if movingUp/movingDown flags are true
        if (movingUp)
            y = y - velocity;
        if (movingDown)
            y = y + velocity;
    }
}
