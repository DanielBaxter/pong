class CollisionManager {

    private float windowWidth;
    private float windowHeight;

    CollisionManager(float windowWidth, float windowHeight) {
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;
    }

    boolean areCollidingHorizontal(Entity a, Entity b) {
        float bmidX = b.x + b.width / 2;
        float bmidY = b.y + b.height / 2;
        return (bmidY > a.y && bmidY < a.y + a.height) &&
                (bmidX > a.x && bmidX < a.x + a.width);
    }

    boolean collidingWithTopWall(Entity e) {
        return e.y < 0;
    }
    boolean collidingWithLeftWall(Entity e) {
        return e.x < 0;
    }
    boolean collidingWithBottomWall(Entity e) { return (e.y + e.height) > windowHeight; }
    boolean collidingWithRightWall(Entity e) { return (e.x + e.width) > windowWidth; }

}


