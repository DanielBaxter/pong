import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

class Game extends JPanel implements ActionListener {

    private float windowWidth, windowHeight;

    private int leftScore = 0;
    private int rightScore = 0;

    private static final int WINNING_SCORE = 5;

    //Ball
    private List<Ball> balls = new ArrayList<>();

    //Paddles
    private Paddle paddleLeft;
    private Paddle paddleRight;

    //Entities (Ball and Paddles)
    private List<Entity> entities = new ArrayList<>();

    //Event Handler
    private PongKeyListener keyListener = new PongKeyListener();
    private CollisionManager collisionManager;

    //Initialise Game Object
    Game() {
        addKeyListener(keyListener);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
        RandomNumberGenerator.init();

        Timer timer = new Timer(5, this);
        timer.setInitialDelay(100);
        timer.start();
    }

    private void setupEntities() {
        //Initialise entities
        balls.add(new Ball(windowWidth / 2 + 5, windowHeight / 2 + 5, 10, 10));
        balls.add(new Ball(windowWidth / 2 + 5, windowHeight / 2 + 5, 10, 10));
        paddleLeft = new Paddle(10, windowHeight / 2 - 35, 10, 70, KeyEvent.VK_W, KeyEvent.VK_S);
        paddleRight = new Paddle(windowWidth - 20, windowHeight / 2 - 35, 10, 70, KeyEvent.VK_UP, KeyEvent.VK_DOWN);

        //Add entities to list
        entities.addAll(balls);
        entities.add(paddleLeft);
        entities.add(paddleRight);
    }


    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (collisionManager == null) {
            windowWidth = getWidth();
            windowHeight = getHeight();
            collisionManager = new CollisionManager(windowWidth, windowHeight);
            setupEntities();
        }

        Graphics2D graphics = (Graphics2D)g;
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        //Each entity draws itself
        for (Entity e: entities)
            e.draw(graphics);

        if (isGameOver()) {
            String winMessage = ((leftScore > rightScore) ? "Left" : "Right" + " Player Wins!!");
            graphics.drawString(winMessage, windowWidth / 2, windowHeight / 2);
        }


        //Show current score
        String score = leftScore + "   :   " + rightScore;
        graphics.drawString(score, windowWidth / 2, 20);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {

        //Reset ball and add to score if ball hits horizontal walls
        for (Ball ball: balls) {
            if (collisionManager.collidingWithLeftWall(ball)) {
                rightScore = rightScore + 1;
                ball.reset();
            } else if (collisionManager.collidingWithRightWall(ball)) {
                leftScore = leftScore + 1;
                ball.reset();
            }

            //Bounce ball off paddles if they collide
            if (collisionManager.areCollidingHorizontal(paddleLeft, ball) || collisionManager.areCollidingHorizontal(paddleRight, ball))
                ball.bounceX();
        }

        if (!isGameOver()) {
            //Update entity positions
            for (Entity e : entities)
                e.update(collisionManager, keyListener.keysPressed);
        }

        //Redraw window (including all entities)
        repaint();
    }

    private boolean isGameOver() {
        return leftScore == WINNING_SCORE || rightScore == WINNING_SCORE;
    }
}
